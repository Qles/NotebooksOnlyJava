package NotebooksOnlyJava.io;

import NotebooksOnlyJava.model.Equipment;
import NotebooksOnlyJava.model.Notebook;
import NotebooksOnlyJava.model.Tablet;

import java.util.Collection;

public class Printer {

    public void printNotebooks(Collection<Equipment> equipments) {
        int count = 0;
        for (Equipment equipment : equipments) {
            if (equipment instanceof Notebook) {
                System.out.println(equipment.toString());
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Brak laptopów w serwisie");
        }
    }

    public void printTablets(Collection<Equipment> equipments) {
        int count = 0;
        for (Equipment equipment : equipments) {
            if (equipment instanceof Tablet) {
                System.out.println(equipment.toString());
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Brak tabletów w serwisie");
        }
    }
}
