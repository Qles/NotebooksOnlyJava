package NotebooksOnlyJava.io.file;

import NotebooksOnlyJava.exception.ExportException;
import NotebooksOnlyJava.exception.ImportException;
import NotebooksOnlyJava.model.NotebooksService;

import java.io.*;

public class SerializableFileManager implements FileManager {
    private static final String FILE_NAME = "Service.txt";

    @Override
    public NotebooksService importData() {
        try (FileInputStream fis = new FileInputStream(FILE_NAME);
             ObjectInputStream ois = new ObjectInputStream(fis);
        ) {
            return (NotebooksService) ois.readObject();
        } catch (FileNotFoundException e) {
            throw new ImportException("Brak pliku " + FILE_NAME);
        } catch (IOException e) {
            throw new ImportException("Błąd odczytu pliku " + FILE_NAME);
        } catch (ClassNotFoundException e) {
            throw new ImportException("Niezgoddny typ danych w "  + FILE_NAME);
        }

    }

    @Override
    public void exportData(NotebooksService notebooksService) {
        try (FileOutputStream fos = new FileOutputStream(FILE_NAME);
             ObjectOutputStream oos = new ObjectOutputStream(fos);
        ) {
            oos.writeObject(notebooksService);
        } catch (FileNotFoundException e) {
            throw new ExportException("Brak pliku " + FILE_NAME);
        } catch (IOException e) {
            throw new ExportException("Nie udało się zapisać danych do pliku " + FILE_NAME);
        }
    }
}
