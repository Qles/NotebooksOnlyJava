package NotebooksOnlyJava.io.file;

import NotebooksOnlyJava.model.NotebooksService;

public interface FileManager {
    NotebooksService importData();
    void exportData(NotebooksService notebooksService);
}
