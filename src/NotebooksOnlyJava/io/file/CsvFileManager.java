package NotebooksOnlyJava.io.file;

import NotebooksOnlyJava.exception.ExportException;
import NotebooksOnlyJava.exception.ImportException;
import NotebooksOnlyJava.exception.InvalidDataException;
import NotebooksOnlyJava.model.*;
import java.io.*;
import java.util.Collection;
import java.util.Scanner;

public class CsvFileManager implements FileManager {
    private static final String FILE_NAME = "ns.csv";

    @Override
    public NotebooksService importData() {
        NotebooksService ns = new NotebooksService();
        importEquipments(ns);
        return ns;
    }

    @Override
    public void exportData(NotebooksService notebooksService) {
        exportEquipments(notebooksService);
    }


    private void exportEquipments(NotebooksService ns) {
        Collection<Equipment> equipments = ns.getEquipments().values();
        exportToCsv(equipments, FILE_NAME);
    }


    private <T extends CsvConvertible> void exportToCsv(Collection<T> collection, String fileName) {
        try (FileWriter fileWriter = new FileWriter(fileName);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            for (T element : collection) {
                bufferedWriter.write(element.toCsv());
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new ExportException("Błąd zapisu danych do pliku " + fileName);
        }
    }

    private Equipment createObjectFromString(String csvText) {
        String[] split = csvText.split(";");
        String type = split[0];
        if (Notebook.TYPE.equals(type)) {
            return createNotebook(split);
        } else if (Tablet.TYPE.equals(type)) {
            return createTablet(split);
        }
        throw new InvalidDataException("Nieznany typ sprzętu: " + type);
    }

    private Notebook createNotebook(String[] data) {
        String brand = data[1];
        String model = data[2];
        String serialNumber = data[3];
        String defect = data[4];
        String repairDescription = data[5];
        double screen = Double.valueOf(data[6]);
        return new Notebook(brand, model, serialNumber, defect, repairDescription, screen);
    }

    private Tablet createTablet(String[] data) {
        String brand = data[1];
        String model = data[2];
        String serialNumber = data[3];
        String defect = data[4];
        String repairDescription = data[5];
        double screen = Double.valueOf(data[6]);
        return new Tablet(brand, model, serialNumber, defect, repairDescription, screen);
    }

    private void importEquipments(NotebooksService notebooksService) {
        try (Scanner fileReader = new Scanner(new File(FILE_NAME))) {
            while (fileReader.hasNextLine()) {
                String line = fileReader.nextLine();
                Equipment equipment = createObjectFromString(line);
                notebooksService.addEquipment(equipment);
            }
        } catch (FileNotFoundException e) {
            throw new ImportException("Brak pliku " + FILE_NAME);
        }
    }
}