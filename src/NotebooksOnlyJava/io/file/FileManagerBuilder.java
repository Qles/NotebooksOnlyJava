package NotebooksOnlyJava.io.file;

import NotebooksOnlyJava.exception.NoSuchFileTypeException;
import NotebooksOnlyJava.io.Reader;


public class FileManagerBuilder {
    private Reader reader;

    public FileManagerBuilder(Reader reader) {
        this.reader = reader;
    }

    public FileManager build() {
        System.out.println("Wybierz format danych");
        FileType fileType = getFileType();
        switch (fileType) {
            case CSV:
                return new CsvFileManager();
            case SERIAL:
                return new SerializableFileManager();
            default:
                throw new NoSuchFileTypeException("Nieobsługiwany typ danych");
        }
    }

    private FileType getFileType() {
        boolean typeOk = false;
        FileType result = null;
        do {
            printTypes();
            String type = reader.getString().toUpperCase();
            try {
                result = FileType.valueOf(type);
                typeOk = true;
            } catch (IllegalArgumentException e) {
                System.out.println("Nieobsługiwany typ danych wybierz ponownie");
            }

        } while (!typeOk);

        return result;
    }

    private void printTypes() {
        for (FileType value : FileType.values()) {
            System.out.println(value.name());
        }
    }
}
