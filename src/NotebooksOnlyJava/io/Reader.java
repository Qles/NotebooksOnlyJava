package NotebooksOnlyJava.io;

import NotebooksOnlyJava.model.Notebook;
import NotebooksOnlyJava.model.Tablet;

import java.util.Scanner;

public class Reader {
    private Scanner input = new Scanner(System.in);

    public void close() {
        input.close();
    }

    public int getInt() {
        try {
            return input.nextInt();
        } finally {
            input.nextLine();
        }
    }

    public String getString() {
        return input.nextLine();
    }

    public Notebook readAndCreateNotebook() {
        System.out.println("Marka");
        String brand = input.nextLine();
        System.out.println("Model");
        String model = input.nextLine();
        System.out.println("Numer Seryjny");
        String sn = input.nextLine();
        System.out.println("Opis uszkodzenia");
        String defect = input.nextLine();
        System.out.println("Opis naprawy");
        String description = input.nextLine();
        System.out.println("Wielkość Matrycy");
        double screen = input.nextDouble();
        input.nextLine();

        return new Notebook(brand, model, sn, defect, description, screen);
    }

    public Tablet readAndCreateTablet() {
        System.out.println("Marka");
        String brand = input.nextLine();
        System.out.println("Model");
        String model = input.nextLine();
        System.out.println("Numer Seryjny");
        String sn = input.nextLine();
        System.out.println("Opis uszkodzenia");
        String defect = input.nextLine();
        System.out.println("Opis naprawy");
        String description = input.nextLine();
        System.out.println("Wielkość Matrycy");
        double screen = input.nextDouble();
        input.nextLine();

        return new Tablet(brand, model, sn, defect, description, screen);
    }
}
