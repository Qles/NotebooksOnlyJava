package NotebooksOnlyJava.exception;

public class EquipmentAlreadyExistsException extends RuntimeException {
    public EquipmentAlreadyExistsException(String message) {
        super(message);
    }
}
