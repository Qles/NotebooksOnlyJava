package NotebooksOnlyJava.exception;

public class NoOptionException extends Exception {
    public NoOptionException(String message) {
        super(message);
    }
}
