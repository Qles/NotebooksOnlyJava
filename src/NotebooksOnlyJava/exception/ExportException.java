package NotebooksOnlyJava.exception;

public class ExportException extends RuntimeException {
    public ExportException(String message) {
        super(message);
    }
}
