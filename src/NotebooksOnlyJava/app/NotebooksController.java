package NotebooksOnlyJava.app;

import NotebooksOnlyJava.exception.ExportException;
import NotebooksOnlyJava.exception.ImportException;
import NotebooksOnlyJava.exception.NoOptionException;
import NotebooksOnlyJava.io.Printer;
import NotebooksOnlyJava.io.Reader;
import NotebooksOnlyJava.io.file.FileManager;
import NotebooksOnlyJava.io.file.FileManagerBuilder;
import NotebooksOnlyJava.model.Notebook;
import NotebooksOnlyJava.model.NotebooksService;
import NotebooksOnlyJava.model.Tablet;


import java.util.InputMismatchException;

public class NotebooksController {
    private Reader reader = new Reader();
    private FileManager fileManager;
    private Printer printer = new Printer();

    private NotebooksService notebooksService;

    NotebooksController() {
        fileManager = new FileManagerBuilder(reader).build();
        try {
            notebooksService = fileManager.importData();
            System.out.println("Zaimportowane dane z pliku");
        } catch (ImportException e) {
            System.out.println(e.getMessage());
            System.out.println("Zainicjowano nową bazę");
            notebooksService = new NotebooksService();
        }
    }

    public void control() {
        Option option;

        do {
            printOptions();
            option = getOption();
            switch (option) {
                case ADD_NOTEBOOK:
                    addNotebook();
                    break;
                case ADD_TABLET:
                    addTablet();
                    break;
                case PRINT_NOTEBOOKS:
                    printNotebooks();
                    break;
                case PRINT_TABLETS:
                    printTablets();
                    break;
                case DELETE_NOTEBOOK:
                    deleteNotebook();
                    break;
                case DELETE_TABLET:
                    deleteTablet();
                    break;
                case EXIT:
                    exit();
                    break;
                default:
                    System.out.println("Nie ma takiej opcji wprowadź ponownie:");
            }
        } while (option != Option.EXIT);
    }

    private Option getOption() {
        boolean optionOk = false;
        Option option = null;
        while (!optionOk) {
            try {
                option = Option.createFromInt(reader.getInt());
                optionOk = true;
            } catch (NoOptionException e) {
                System.out.println(e.getMessage() + ", podaj ponownie");
            } catch (InputMismatchException ignored) {
                System.out.println("Wprowadzono wartość która nie jest liczbą, podaj ponownie");
            }
        }
        return option;
    }

    private void printOptions() {
        System.out.println("Wybierz opcję");
        for (Option option : Option.values()) {
            System.out.println(option);
        }
    }

    private void addNotebook() {
        try {
            Notebook notebook = reader.readAndCreateNotebook();
            notebooksService.addEquipment(notebook);
        } catch (InputMismatchException e) {
            System.out.println("Nie udało się dodać Laptopa, niepoprawne dane");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Osiągnieto limit pojemności, nie można dodać kolejnego laptopa");
        }
    }

    private void printNotebooks() {
        printer.printNotebooks(notebooksService.getEquipment());

    }

    private void addTablet() {
        try {
            Tablet tablet = reader.readAndCreateTablet();
            notebooksService.addEquipment(tablet);
        } catch (InputMismatchException e) {
            System.out.println("Nie udało się dodać Tableta, niepoprawne dane");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Osiągnieto limit pojemności, nie można dodać kolejnego tableta");
        }
    }

    private void printTablets() {
        printer.printTablets(notebooksService.getEquipment());
    }

    private void deleteNotebook() {
        try {
            Notebook notebook = reader.readAndCreateNotebook();
            if (notebooksService.removeEquipment(notebook))
                System.out.println("Usunięto laptop");
            else
                System.out.println("Brak takiego laptopa");
        } catch (InputMismatchException e) {
            System.out.println("Nie udało się utworzyć laptopa niepoprawne dane");
        }
    }

    private void deleteTablet() {
        try {
            Tablet tablet = reader.readAndCreateTablet();
            if (notebooksService.removeEquipment(tablet))
                System.out.println("Usunięto tablet");
            else
                System.out.println("Brak takiego tabletu");
        } catch (InputMismatchException e) {
            System.out.println("Nie udało się utworzyć tabletu niepoprawne dane");
        }
    }

    private void exit() {
        try {
            fileManager.exportData(notebooksService);
            System.out.println("Export danych do pliku zakończony powodzeniem");
        } catch (ExportException e) {
            System.out.println(e.getMessage());
        }
        reader.close();
        System.out.println("Wychodzisz z serwisu");
    }
}