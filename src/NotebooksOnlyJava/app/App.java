package NotebooksOnlyJava.app;

public class App {
    private static final String APP_NAME = "Serwis laptopów";
    public static void main(String[] args) {

        System.out.println(APP_NAME);
        NotebooksController notebooksController = new NotebooksController();
        notebooksController.control();
    }
}
