package NotebooksOnlyJava.app;

import NotebooksOnlyJava.exception.NoOptionException;

public enum Option {
    EXIT(0, "Wyjście z programu"),
    ADD_NOTEBOOK(1, "Dodanie Laptopa"),
    ADD_TABLET(2,"Dodanie Tableta"),
    PRINT_NOTEBOOKS(3, "Wyświetlenie dostępnych Laptopów"),
    PRINT_TABLETS(4, "Wyświetlenie dostępnych Tabletów"),
    DELETE_NOTEBOOK(5, "Usuń laptop"),
    DELETE_TABLET(6, "Usuń Tablet");

    private int value;
    private String description;

    Option(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return value + " " + description;
    }

    static Option createFromInt(int option) throws NoOptionException {
        try{
            return Option.values()[option];
        }catch (ArrayIndexOutOfBoundsException e){
            throw new NoOptionException("Brak opcji o id " + option);
        }
    }
}
