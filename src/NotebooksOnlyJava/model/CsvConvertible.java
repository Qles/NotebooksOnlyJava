package NotebooksOnlyJava.model;

public interface CsvConvertible {
    String toCsv();
}
