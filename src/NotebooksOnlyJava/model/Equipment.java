package NotebooksOnlyJava.model;

import java.io.Serializable;
import java.util.Objects;

public abstract class Equipment implements Serializable, CsvConvertible {

    public abstract String toCsv();

    private String brand;
    private String model;
    private String serialNumber;
    private String defect;
    private String repairDescription;

    public Equipment(String brand, String model, String serialNumber, String defect, String repairDescription) {
        this.brand = brand;
        this.model = model;
        this.serialNumber = serialNumber;
        this.defect = defect;
        this.repairDescription = repairDescription;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDefect() {
        return defect;
    }

    public void setDefect(String defect) {
        this.defect = defect;
    }

    public String getRepairDescription() {
        return repairDescription;
    }

    public void setRepairDescription(String repairDescription) {
        this.repairDescription = repairDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Equipment equipment = (Equipment) o;
        return Objects.equals(brand, equipment.brand) &&
                Objects.equals(model, equipment.model) &&
                Objects.equals(serialNumber, equipment.serialNumber) &&
                Objects.equals(defect, equipment.defect) &&
                Objects.equals(repairDescription, equipment.repairDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, serialNumber, defect, repairDescription);
    }

    @Override
    public String toString() {
        return brand + ", " + model + ", " + serialNumber + ", " + defect + ", " + repairDescription;

    }
}
