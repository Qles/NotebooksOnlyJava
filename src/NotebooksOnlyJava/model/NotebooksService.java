package NotebooksOnlyJava.model;


import NotebooksOnlyJava.exception.EquipmentAlreadyExistsException;
import java.io.Serializable;
import java.util.*;

public class NotebooksService implements Serializable {

    private Map<String, Equipment> equipments = new HashMap<>();

    public Map<String, Equipment> getEquipments() {
        return equipments;
    }

    public Collection<Equipment> getEquipment() {
        ArrayList<Equipment> list = new ArrayList<>(this.equipments.values());
        return list;
    }

    public void addEquipment(Equipment equipment) {
        if (equipments.containsKey(equipment.getSerialNumber()))
            throw new EquipmentAlreadyExistsException(
                    "Sprzęt o takim numerze Seryjnym już istnieje " + equipment.getSerialNumber()
            );
        equipments.put(equipment.getSerialNumber(), equipment);
    }

    public boolean removeEquipment(Equipment equipment) {
        if (equipments.containsValue(equipment)) {
            equipments.remove(equipment.getSerialNumber());
            return true;
        } else {
            return false;
        }
    }
}