package NotebooksOnlyJava.model;

import java.util.Objects;

public class Tablet extends Equipment {
    public static final String TYPE = "TABLET";
    private double screen;

    public Tablet(String brand, String model, String serialNumber, String defect, String repairDescription, double screen) {
        super(brand, model, serialNumber, defect, repairDescription);
        this.screen = screen;
    }

    public double getScreen() {
        return screen;
    }

    public void setScreen(double screen) {
        this.screen = screen;
    }

    @Override
    public String toCsv() {
        return (TYPE + ";") +
                getBrand() + ";" +
                getModel() + ";" +
                getSerialNumber() + ";" +
                getDefect() + ";" +
                getRepairDescription() + ";" +
                getScreen() + "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Tablet tablet = (Tablet) o;
        return Double.compare(tablet.screen, screen) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), screen);
    }

    @Override
    public String toString() {
        return super.toString() + "," + screen;
    }
}
